FROM python:3.10

RUN apt-get update && apt-get install -y \
    postgresql \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
# ENV PATH="/root/.local/bin:${PATH}"

WORKDIR /code

COPY poetry.lock pyproject.toml /code/

RUN /root/.local/bin/poetry install --only main --no-root

COPY . /code/

EXPOSE 8000

CMD ["/root/.local/bin/poetry", "run", "python", "manage.py", "runserver"]