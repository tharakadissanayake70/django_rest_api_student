from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.getStudents, name="api-getdata"),
    path('add/', views.addStudent, name="api-adddata"),
]