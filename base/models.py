from django.db import models

# Create your models here.

class Student(models.Model):
    ad_no = models.CharField(max_length=4)
    name = models.CharField(max_length=50)
    dob = models.DateField()
    gender = models.CharField(max_length=6)
    city = models.CharField(max_length=50)

